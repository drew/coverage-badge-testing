# Badges

## Pipeline

![pipeline](https://gitlab.com/drewcimino/coverage-badge-testing/badges/master/pipeline.svg)

## Coverage: no job specified

![coverage](https://gitlab.com/drewcimino/coverage-badge-testing/badges/master/coverage.svg)

Because the badge request URL does not specify a job,

```
https://gitlab.com/drewcimino/coverage-badge-testing/badges/master/coverage.svg
```

the badge returns the pipeline coverage (average of all builds) from the most recent
[successful pipeline](https://gitlab.com/drewcimino/coverage-badge-testing/-/pipelines/217420122).

## Coverage: `rspec-coverage` specified

![rspec coverage](https://gitlab.com/drewcimino/coverage-badge-testing/badges/master/coverage.svg?job=rspec-coverage)

Here, the badge request url includes a specific job title as a query string parameter:

```
https://gitlab.com/drewcimino/coverage-badge-testing/badges/master/coverage.svg?job=rspec-coverage
```

and so it uses coverage from the `rspec-coverage` build in a more recent, but blocked and technically
[running pipeline](https://gitlab.com/drewcimino/coverage-badge-testing/-/pipelines/217441930).
